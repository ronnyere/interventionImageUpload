<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Auth;
use Image;

class UserController extends Controller
{
    //
    public function profile(){
        return view('profile', array('user' => Auth::user()) );
    }

    public function update_avatar(Request $request){

        if($request->hasFile('avatar')){
            $user            = Auth::user();
            $old_avatar      = $user->avatar;
            $file            = $request->file('avatar');
            $filename        = time() . '.' . $file->getClientOriginalExtension();
            $old_file_avatar = $old_avatar  . '.' . $file->getClientOriginalExtension();
            $image           = Image::make($file);

            $image->fit(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::put($filename, (string) $image->encode());

            $user->avatar = $filename;
            $user->save();

        }

        //delete image
        if (Auth::user() !== $filename) {
            Storage::delete($old_avatar);
        }


        return redirect('profile');
    }

    public function getUserImage()
    {
        $user        = Auth::user();
        $old_avatar  = $user->avatar;
        $file        = Storage::disk('local')->get($old_avatar);
        return Response::make($file,200,[ 'Content-Type' => $old_avatar]);


    }

}
